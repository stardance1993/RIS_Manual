# 第一节    检查部位维护

1. 打开本系统，找到右下托盘中的工作站图标，选择*检查部位*菜单。

   ![](../Images/检查部位#上下文菜单.jpg)

2. 在打开的页面中，点击`新增`按钮，在弹出的界面中，输入部位名称，点击`确定`保存更改。

   ![](../Images/检查类型#创建.jpg)

