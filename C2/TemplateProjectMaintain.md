# 第四节    模版项目维护

1. 在右下方托盘菜单中，定位工作站，右键弹出托盘菜单,选择*模版设置*。

   ![](../Images/模版设置.png)

2. 点击*个人*筛选框，如果想在哪个模版类别中添加模版，点击模版分类，比如*泌尿*，再点击`新增`,弹出新增对话框。

   ![](../Images/添加模版.jpg)

3. 如图所示，依次编辑**模版名称**，**检查所见**，**诊断结果**，并点击`确定`保存更改。

   ![](../Images/编辑模版内容.jpg)